import numpy as np
import pandas as pd
from sklearn.naive_bayes import GaussianNB
from sklearn.model_selection import train_test_split
from sklearn import metrics 

df = pd.read_csv('resources/diabetes.csv')

features = df[df.columns[:8]]
result = df[['Outcome']]

X_train, X_test, y_train, y_test = train_test_split(features, result, test_size=0.4) 
gnb = GaussianNB() 
gnb.fit(X_train, y_train) 
testing_data = []

ip = int(input("Enter the number of pregnancies:\t"))
testing_data.append(ip)
ip = float(input("Enter glucose level:\t"))
testing_data.append(ip)
ip = float(input("Enter BP level:\t"))
testing_data.append(ip)
ip = float(input("Enter skin thickness:\t"))
testing_data.append(ip)
ip = float(input("Enter insulin level:\t"))
testing_data.append(ip)
ip = float(input("Enter BMI level:\t"))
testing_data.append(ip)
ip = float(input("Enter DPF level:\t"))
testing_data.append(ip)
ip = float(input("Enter Age:\t"))
testing_data.append(ip)

new_df = pd.DataFrame(columns = [x for x in df.columns[:8]])
new_df.loc[1] = testing_data

y_pred_given = gnb.predict(new_df)

if y_pred_given[0] == 1:
	print("Patient has diabetes")
else:
	print("Patient does not have diabetes") 

y_pred = gnb.predict(X_test)

print("Accuracy = ",round((metrics.accuracy_score(y_test, y_pred)*100),2),'%')

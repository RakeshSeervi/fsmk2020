import numpy as np
import pandas as pd
from bokeh.plotting import figure, output_file, show
from bokeh.layouts import column

df = pd.read_csv('resources/diabetes.csv')

haveDiabetes = df[df['Outcome'] == 1]	#filtering out women having diabetes


#for plotting a graph of number of pregnancies vs women having diabetes
 
yValues = haveDiabetes['Pregnancies'].value_counts().to_dict()	#counting the number of women having a particular number of pregnancies	

#x-axis values
x = np.arange(18)	

#y-axis values
y = []

for i in range(18):
	try:
		y.append(yValues[i])
	except:
		y.append(0)

p = figure(x_axis_label = "No of pregnancies", y_axis_label = "No of women having diabetes")
p.vbar(x = x, top = y, width = 0.5, bottom = 0 )


#for plotting a graph of number of pregnancies vs women having diabetes

yValues = haveDiabetes['Glucose'].value_counts().to_dict()	#counting the number of women having a particular glucose level

#y-axis values
y = []

for i in range(61, 200, 20):
    s = 0
    for j in range(i, i + 20):
        try:
            s += yValues[j]
        except:
            s += 0
    y.append(s)

#x-axis values
x = ['61 - 80', '81 - 100', '101 - 120', '121 - 140', '141 - 160', '161 - 180', '181 - 200']

q = figure(x_range = x, x_axis_label = "Range of glucose level", y_axis_label = "No of women having diabetes")
q.vbar(x = x, top = y, width = 0.5, bottom = 0)

show(column(p,q))


